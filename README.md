# Prompt

Create custom bash prompts.

![screen](screen.png)


## Install
`
$ git clone https://gitlab.com/mauruca/prompt.git ~/.prompt && ~/.prompt/install.sh
`
<br/>
`
$ source ~/.bashrc
`

## Uninstall
`
$ ~/.prompt/uninstall.sh
`
