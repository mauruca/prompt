#!/bin/bash
path=${0%/*}

cp $path/prompt ~
#remove duplications
sed -i '/source ~\/prompt/d' ~/.bashrc
#install
echo "source ~/prompt" >> ~/.bashrc
echo ""
echo "run $ source ~/.bashrc"
